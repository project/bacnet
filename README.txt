
CONTENTS OF THIS FILE
---------------------

 * About BACNET
 * Configuration and features
 * Installation profiles
 * Appearance
 * Developing for BACNET

ABOUT BACNET
------------

The BACnet module integrates Energy content from building systems into a variety of websites ranging from personal websites to large community-driven websites.  For more information see the BACnet project page at http://drupal.org/project/drupal, and get involved with the BACnet-Drupal community at http://bg-drupal.org

CONFIGURATION AND FEATURES
--------------------------

The BACnet module enables the display of energy related data from BACnet systems to many, many people, by putting the data right into the sites they already go to many times a day.

The module is setup as a suite of 4 energy dashboard features, each providing a particular type of functionality:

1. Text - For example, the value NOW .  This allows comparison of current electrical usage between two buildings.


2. Dials - The Dial image is a way to display the NOW value in the context of an upper limit set for the facility.

3. Charts - Bar Charts display data over a period of time, for instance average zone temperature computed hourly and displayed for the last 24 hours.

4. Graphs - Line Graphs, like Charts, displays data over a period of time, and are useful for displays such as comparing electrical usage between facilities.

* Install as usual, see 
https://www.drupal.org/documentation/install/modules-themes/modules-7 
for further information.
  
*  Note that installing external libraries is 
separate from installing this module 
and should happen in the sites/all/libraries directory. 
See http://drupal.org/node/1440066 for more information.

Requirements:

* Drupal 7.x
* Block
* Chaos tool suite (ctools)
* Views
* Libraries 2.0
* Flots 0.7 library  (http://www.flotcharts.org

-- Libraries Installation --
Copy each of the following libraries 
into their respective folder  
(i.e. flot and transform) 
in the libraries folder for the module to detect the libraries correctly
* Flots 0.7 library  
(see https://code.google.com/p/flot/downloads/detail?name=flot-0.7.zip&can=2&q=)
* Jquery transform 0.9.3 library 
( see http://wiki.github.com/heygrady/transform/ ) 


INSTALLATION PROFILE
--------------------

Work on an installation profile incorporating the BACnet module, called OpenEnergy, began a couple years ago. Like the BACnet module, since it displays real-time data from real-time systems, Drupal developers needed access to the OEM real-time systems to develop against.  BG-Drupal sought sponsors to make systems available for development purpose.

DEVELOPING FOR BACNET
---------------------

As of Jan 2017 new systems have been brought on-line specifically for development purposes and are managed directly by http://bg-drupal.org.  Experienced Drupal volunteers (frontend & backend) now have easy access to industrial grade  BACnet systems. 

CONTACT
-------

Current maintainers:

* David Thompson (dbt102)
https://www.drupal.org/user/338300


